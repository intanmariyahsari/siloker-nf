<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="">
<meta name="author" content="">
<link rel="preconnect" href="https://fonts.gstatic.com">
<!--<link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@300;400;600;700;800&display=swap" rel="stylesheet">-->
<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet">

<title>SILOKER-NF</title>

<!-- Bootstrap core CSS -->
<link href="<?= base_url("public/vendor/bootstrap/css/bootstrap.min.css") ?>" rel="stylesheet">


<!-- Additional CSS Files -->
<link rel="stylesheet" href="<?= base_url("public/assets/css/fontawesome.css") ?>">
<link rel="stylesheet" href="<?= base_url("public/assets/css/style.css") ?>">
<link rel="stylesheet" href="<?= base_url("public/assets/css/animated.css") ?>">
<link rel="stylesheet" href="<?= base_url("public/assets/css/owl.css") ?>">
<link rel="stylesheet" href="<?= base_url("public/form/css/style.css") ?>">
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.25/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">