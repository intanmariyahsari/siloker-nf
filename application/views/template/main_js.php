 <!-- Scripts -->
 <script src="<?= base_url("public/vendor/jquery/jquery.min.js") ?>"></script>
 <script src="<?= base_url("public/vendor/bootstrap/js/bootstrap.bundle.min.js") ?>"></script>
 <script src="<?= base_url("public/assets/js/owl-carousel.js") ?>"></script>
 <script src="<?= base_url("public/assets/js/animation.js") ?>"></script>
 <script src="<?= base_url("public/assets/js/imagesloaded.js") ?>"></script>
 <script src="<?= base_url("public/assets/js/custom.js") ?>"></script>
 <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
 <script src="https://cdn.datatables.net/1.10.25/js/jquery.dataTables.min.js"></script>
 <script src="https://cdn.datatables.net/1.10.25/js/dataTables.bootstrap4.min.js"></script>
 <script>
     $(document).ready(function() {
         $('#example').DataTable();
     });
 </script>