<!-- ***** Preloader Start ***** -->
<div id="js-preloader" class="js-preloader">
	<div class="preloader-inner">
		<span class="dot"></span>
		<div class="dots">
			<span></span>
			<span></span>
			<span></span>
		</div>
	</div>
</div>
<!-- ***** Preloader End ***** -->



<div class="main-banner wow fadeIn" id="top" data-wow-duration="1s" data-wow-delay="0.5s">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="row" style="margin-top: -66px;">
					<div class="col-lg-6 align-self-center">
						<div class="left-content header-text wow fadeInLeft" data-wow-duration="1s" data-wow-delay="1s">
							<div class="row">
								<div class="col-lg-6">
									<div class="info-stat">
										<h3 style="font-size: 21px;">Sistem Lowongan Kerja</h3>
									</div>
								</div>

								<div class="col-lg-12">
									<h2 id="font-dpn">STT Nurul Fikri</h2>
								</div>
								<div class="col-lg-12">
									<div class="main-green-button scroll-to-section">
										<a href="#contact">Tentang SILOKER</a>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-lg-6">
						<div class="right-image wow fadeInRight" data-wow-duration="1s" data-wow-delay="0.5s">
							<img src="<?= base_url("public/assets/images/icon-dpn.svg") ?>" alt="" id="icon-dpn">
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div id="services" class="our-services section" style="margin-top: -146px;">
	<div class="container">
		<div class="row">
			<div class="col-lg-6 offset-lg-3">
				<div class="section-heading wow bounceIn" data-wow-duration="1s" data-wow-delay="0.2s">
					<h2>Lowongan <em>Kerja</em></h2>
				</div>
			</div>
		</div>
	</div>
	<div class="container-fluid">
		<div class="row">

			<?php $i = 0 ?>
			<?php foreach ($lowongan as $lowongan) { ?>
				<?php $i++ ?>

				<div class="col-lg-4">
					<div class="service-item wow bounceInUp" data-wow-duration="1s" data-wow-delay="0.3s">
						<div class="row">
							<div class="col-lg-4">
								<div class="icon">
									<img src="<?= base_url("upload/") . $lowongan->logo ?>" alt="">
								</div>
							</div>
							<div class="col-lg-8">
								<div class="right-content">
									<h4><?= character_limiter($lowongan->nama, 13) ?></h4>
									<p><?= character_limiter($lowongan->deskripsi_pekerjaan, 20) ?></p>
									<p><?= $lowongan->tanggal_akhir ?>, <a href="<?= base_url("dashboard/detailLowongan/") . $lowongan->id ?>">Lihat Detail</a></p>
								</div>
							</div>
						</div>
					</div>
				</div>

				<?php if ($i == 3) {
					break;
				} ?>

			<?php } ?>

		</div>
	</div>
</div>
</div>

<div id="portfolio" class="our-portfolio section">
	<div class="container">
		<div class="row">
			<div class="col-lg-5">
				<div class="section-heading wow bounceIn" data-wow-duration="1s" data-wow-delay="0.2s">
					<h2>Daftar <em>Mitra</em></h2>
				</div>
			</div>
		</div>
	</div>
	<div class="container-fluid wow fadeIn" data-wow-duration="1s" data-wow-delay="0.7s">
		<div class="row">
			<div class="col-lg-12">
				<div class="loop owl-carousel">
					<?php foreach ($mitra as $mitra) { ?>
						<div class="item">
							<div class="portfolio-item">

								<div class="thumb">
									<img src="<?= base_url("upload/") . $lowongan->logo ?>" alt="">
									<div class="hover-content">
										<div class="inner-content">
											<a href="#">
												<h4><?= $mitra->nama ?></h4>
											</a>
										</div>
									</div>
								</div>

							</div>
						</div>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>

	<!-- <div id="contact" class="contact-us section">
	<div class="container">
		<div class="row">
			<div class="col-lg-12 wow fadeInUp" data-wow-duration="0.5s" data-wow-delay="0.25s">
				<form id="contact" action="" method="post">
					<div class="row">
						<div class="col-lg-6 offset-lg-3">
							<div class="section-heading">
								<h6>Contact Us</h6>
								<h2>Fill Out The Form Below To <span>Get</span> In <em>Touch</em> With Us</h2>
							</div>
						</div>
						<div class="col-lg-9">
							<div class="row">
								<div class="col-lg-6">
									<fieldset>
										<input type="name" name="name" id="name" placeholder="Name" autocomplete="on" required>
									</fieldset>
								</div>
								<div class="col-lg-6">
									<fieldset>
										<input type="surname" name="surname" id="surname" placeholder="Surname" autocomplete="on" required>
									</fieldset>
								</div>
								<div class="col-lg-6">
									<fieldset>
										<input type="text" name="email" id="email" pattern="[^ @]*@[^ @]*" placeholder="Your Email" required="">
									</fieldset>
								</div>
								<div class="col-lg-6">
									<fieldset>
										<input type="subject" name="subject" id="subject" placeholder="Subject" autocomplete="on">
									</fieldset>
								</div>
								<div class="col-lg-12">
									<fieldset>
										<textarea name="message" type="text" class="form-control" id="message" placeholder="Message" required=""></textarea>
									</fieldset>
								</div>
								<div class="col-lg-12">
									<fieldset>
										<button type="submit" id="form-submit" class="main-button ">Send Message Now</button>
									</fieldset>
								</div>
							</div>
						</div>
						<div class="col-lg-3">
							<div class="contact-info">
								<ul>
									<li>
										<div class="icon">
											<img src="public/assets/images/contact-icon-01.png" alt="email icon">
										</div>
										<a href="#">info@company.com</a>
									</li>
									<li>
										<div class="icon">
											<img src="public/assets/images/contact-icon-02.png" alt="phone">
										</div>
										<a href="#">+001-002-0034</a>
									</li>
									<li>
										<div class="icon">
											<img src="public/assets/images/contact-icon-03.png" alt="location">
										</div>
										<a href="#">26th Street, Digital Villa</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>-->
	<style>
		#icon-dpn {
			margin-top: 45px;
			width: 724px;
			margin-left: -61px;
		}

		#font-dpn {
			font-size: 50px;
		}
	</style>